# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarDSSATSetup1():

  _Section_Name = 'DSSATSetup1'    ## _single_leading_underscore => private variable
  # Station with Historical Weather Data
  WStation = None
  # avail_year1 = None
  # avail_year2 = None
  # sim_year1 = None #simulation start year
  # sim_year2 = None #simulation end year

  #Seasonal climate forecast
  NN1 = None
  NN2 = None
  AN1 = None
  AN2 = None
  ML1 = None
  ML2 = None
  trimester1 = None
  trimester2 = None

  #Planting date
  plt_year = None
  plt_month = None
  plt_date = None

  #Crop type
  Crop_type = None # RButton2
# ===1) crop informatoin for maize from dialog
  #Maize input
  PN_soil_type = None  
  PN_cul_type = None  
  PN_ini_H2O = None  
  PN_ini_NO3 = None 
  # PN_plt_date = None 

  #Drybean input
  ML_soil_type = None  
  ML_cul_type = None  
  ML_ini_H2O = None  
  ML_ini_NO3 = None 
  # ML_plt_date = None 

  # Sorghum input
  SG_soil_type = None
  SG_cul_type = None
  SG_ini_H2O = None
  SG_ini_NO3 = None
  # ML_plt_date = None

  # from main page
  Soil_Type = None #from main page
  CUL_Cal_CulName = None # calib_cul
  IC_H2O = None
  IC_NO3 = None
  Plt_date = None

  def __init__(self):
    pass

  def initVars(self):
    pass

  # load from configuration file
  def loadConfig(self):
    #weather station
    get_config_value_into_pmw_combobox(self._Section_Name, 'WStation', self.WStation) 
    get_config_value_into_tkinter_label(self._Section_Name, 'avail_year1', self.avail_year1) # available start year of the selected station
    get_config_value_into_tkinter_label(self._Section_Name, 'avail_year2', self.avail_year2)   # available last year of the selected station
    get_config_value_into_pmw_entryfield(self._Section_Name, 'sim_year1', self.sim_year1) #sim start year
    get_config_value_into_pmw_entryfield(self._Section_Name, 'sim_year2', self.sim_year2) #sim end year

    # # crop to plant
    get_config_value_into_tkinter_intvar(self._Section_Name, 'Crop_type', self.Crop_type) #

    #from labels
    get_config_value_into_tkinter_label(self._Section_Name, 'Soil_Type', self.Soil_Type)
    get_config_value_into_tkinter_label(self._Section_Name, 'CUL_Cal_CulName', self.CUL_Cal_CulName)
    get_config_value_into_tkinter_label(self._Section_Name, 'IC_H2O', self.IC_H2O)
    get_config_value_into_tkinter_label(self._Section_Name, 'IC_NO3', self.IC_NO3)
    get_config_value_into_tkinter_label(self._Section_Name, 'Plt_date', self.Plt_date)

    # # from PN dialog
    # get_config_value_into_pmw_combobox(self._Section_Name, 'PN_soil_type', self.PN_soil_type) #soil_type
    # get_config_value_into_pmw_combobox(self._Section_Name, 'PN_cul_type', self.PN_cul_type) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'PN_ini_H2O', self.PN_ini_H2O) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'PN_ini_NO3', self.PN_ini_NO3) 
    # get_config_value_into_pmw_entryfield(self._Section_Name,'PN_plt_date', self.PN_plt_date)         

    # # from ML dialog
    # get_config_value_into_pmw_combobox(self._Section_Name, 'ML_soil_type', self.ML_soil_type) #soil_type
    # get_config_value_into_pmw_combobox(self._Section_Name, 'ML_cul_type', self.ML_cul_type) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'ML_ini_H2O', self.ML_ini_H2O) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'ML_ini_NO3', self.ML_ini_NO3) 
    # get_config_value_into_pmw_entryfield(self._Section_Name, 'ML_plt_date', self.ML_plt_date)  

 # save into configration file
  def saveConfig(self):
    #weather station
    set_config_value_from_pmw_combobox(self._Section_Name, 'WStation', self.WStation) 
    set_config_value_from_tkinter_label(self._Section_Name, 'avail_year1', self.avail_year1 ) # available start year of the selected station
    set_config_value_from_tkinter_label(self._Section_Name, 'avail_year2', self.avail_year2)   # available last year of the selected station
    set_config_value_from_pmw_entryfield(self._Section_Name, 'sim_year1', self.sim_year1) #sim start year
    set_config_value_from_pmw_entryfield(self._Section_Name, 'sim_year2', self.sim_year2) #sim end year

    # # crop to plant
    set_config_value_from_tkinter_intvar(self._Section_Name, 'Crop_type', self.Crop_type) 

    #from labels
    set_config_value_from_tkinter_label(self._Section_Name, 'Soils_Type', self.Soil_Type)
    set_config_value_from_tkinter_label(self._Section_Name, 'CUL_Cal_CulName', self.CUL_Cal_CulName)
    set_config_value_from_tkinter_label(self._Section_Name, 'IC_H2O', self.IC_H2O)
    set_config_value_from_tkinter_label(self._Section_Name, 'IC_NO3', self.IC_NO3)
    set_config_value_from_tkinter_label(self._Section_Name, 'Plt_date', self.Plt_date)

    # # from PN dialog
    # set_config_value_from_pmw_combobox(self._Section_Name, 'PN_soil_type', self.PN_soil_type) #soil_type
    # set_config_value_from_pmw_combobox(self._Section_Name, 'PN_cul_type', self.PN_cul_type) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'PN_ini_H2O', self.PN_ini_H2O) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'PN_ini_NO3', self.PN_ini_NO3) 
    # set_config_value_from_pmw_entryfield(self._Section_Name, 'PN_plt_date', self.PN_plt_date)         

    # # from ML dialog
    # set_config_value_from_pmw_combobox(self._Section_Name, 'ML_soil_type', self.ML_soil_type) #soil_type
    # set_config_value_from_pmw_combobox(self._Section_Name, 'ML_cul_type', self.ML_cul_type) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'ML_ini_H2O', self.ML_ini_H2O) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'ML_ini_NO3', self.ML_ini_NO3) 
    # set_config_value_from_pmw_entryfield(self._Section_Name, 'ML_plt_date', self.ML_plt_date)  

