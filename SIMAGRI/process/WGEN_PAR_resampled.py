#Author: Eunjin Han updated (2/23/2020)
#Author: Eunjin Han updated (2/23/2020)
# for resampling 4 different periods in 2 yrs span
#1)climatoloy, 2) first trimester for SCF1, 3) second trimester for SCF2, 4)climatology for remaining period
# therefore, this code writes two parameter_input.txt files for YR1 and YR2
#=================================
#Author: Eunjin Han
#Institute: IRI-Columbia University, Date: 5/20/2019
#Purpose: Improve Richardson(1984)'s weather generator so as to
# 1) link with seasonal climate forecast provided in tercile format for each individual month (e.g., PAGASA's)
# 2) parameters of rainfall model and temperature/srad are estimated based on resampled "n" years of weather data
# 3) Monthly target values are estimated to reflect interannual variability (low-frequency)
#    Here, monthly target values (5%~95%) are extracted from the CDF curves of ""resampled weather realizations"""
#          not from historical observations. (5/24/2019)

import numpy as np
import math
import scipy
import pandas as pd
import calendar
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
from scipy.optimize import curve_fit
import time
import matplotlib.pyplot as plt
from SIMAGRI.process.write_WGEN_input import write_WGEN_input


start_time = time.clock()
#=================================================================

def WGEN_PAR(WTD_df,df_scf,indices1, indices2, wdir, station_name, ALAT, target_year):
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    #m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    # numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================
    year_array = WTD_df.YEAR.unique()
    nrealiz = 500 #===> Hard coded

    temp = df_scf.loc["SCF"].values
    SCF1_loc = np.where(temp == 1) #SCF1 
    SCF2_loc = np.where(temp == 2) #SCF2   

    #case 1: SCF1 goes beyond to YR2, SCF2 is in YR2
    if SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11: #in case of SCF1 is for N(11th) DJ or D(12th)JF
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]  #-1  #exclude one last year because SCF1 is for NDJ
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations
        
        #================== YR1 ======================
        #make empty arrays to save daily weather data for period 1 in YR1 before SCF1
        ndays = m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices2[rand_index[ii]] #**Note: indeces are from indices2!!!
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 2 based on SCF1
        nyears = year_array.shape[0]-1 #counter by excluding the last observed year
        # year_array = year_array[0:-1]  #remove the last observed year
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        # NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1" only => i.e., ND only for NDJ
        ndays = 365 - m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_doy = m_doys_list2[SCF1_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_doy = m_doys_list[SCF1_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = m_doys_list[SCF1_loc[0][0]]-1  #start day
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input (rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)

        #================== YR2 ======================
        #make empty arrays to save resampled daily data for YR2
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN
        #make empty arrays to save daily weather data for period 2 in "YR2" only => i.e., J in YR2 only for NDJ
        ndays = m_doye_list[SCF1_loc[0][2]-1]  #*****************
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            # if calendar.isleap(year_array[j]): #skip Feb. 29
            #     s_doy = m_doys_list2[SCF1_loc[0][0]]
            Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            # else: #normal year
            #     s_doy = m_doys_list[SCF1_loc[0][0]]
            #     Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][0:ndays] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 3)resample for period 3 based on SCF2
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        # NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 3 in "YR2"
        ndays = m_doye_list[SCF2_loc[0][2]-12] - m_doye_list[SCF2_loc[0][0]-13]  #for FMA: end day of April[120] - end day of Jan[31]   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                if SCF2_loc[0][0]-12 == 1:   #If SCF2 is for FMA   #=> 13th index -12 => month #2 (Feb)=> index 1
                    #for Feb 1 to 28th
                    Rain_temp[j,0:28] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    Tmin_temp[j,0:28] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    Tmax_temp[j,0:28] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    SRad_temp[j,0:28] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    #from March 1~April 30
                    Rain_temp[j,28:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    Tmin_temp[j,28:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    Tmax_temp[j,28:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    SRad_temp[j,28:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                elif SCF2_loc[0][0]-12 > 1: #If SCF2 is for MAM
                    s_doy = m_doys_list2[SCF2_loc[0][0]-12]
                    e_doy = m_doye_list2[SCF2_loc[0][2]-12]  #check!
                    Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = m_doys_list[SCF2_loc[0][0]-12]
                e_doy = m_doye_list[SCF2_loc[0][2]-12]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR2 matrix with SAMPLES 
        s_doy = m_doys_list[SCF2_loc[0][0]-12]-1  #start day index
        e_doy = m_doye_list[SCF2_loc[0][2]-12]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 4)resample for period 4 for the remaining months in YR2
        #make empty arrays to save daily weather data for period 4 (remaining days in YR4)
        ndays = 365 - e_doy #m_doye_list[SCF2_loc[0][2]-12]  #check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_date = m_doye_list2[SCF2_loc[0][2]-12]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_date = m_doye_list[SCF2_loc[0][2]-12]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices2[rand_index[ii]] #**Note: indeces are from indices2!!!
            rain_resampled[ii][s_date-1:] = Rain_temp[temp_index,:]
            tmax_resampled[ii][s_date-1:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_date-1:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_date-1:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year+1)

#=============================================================
#++++ case 2: SCF1 is in YR1 but SCF2 goes beyond to YR2
#=============================================================
    elif SCF2_loc[0][0] == 10 or SCF2_loc[0][0] == 11: #in case of SCF2 is for NDJ or DJF
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations
        
        #================== YR1 ======================
        #make empty arrays to save daily weather data for period 1 in YR1 before SCF1
        ndays = m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29 [doy = 60]
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 2 based on SCF1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        # NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1"
        ndays = m_doye_list[SCF1_loc[0][2]] - m_doye_list[SCF1_loc[0][0]-1]  #e.g., for SON: end day of Nov[334] - end day of Aug[243]   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_doy = m_doys_list2[SCF1_loc[0][0]]
                e_doy = m_doye_list2[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = m_doys_list[SCF1_loc[0][0]]
                e_doy = m_doye_list[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = m_doys_list[SCF1_loc[0][0]]-1  #start day index
        e_doy = m_doye_list[SCF1_loc[0][2]]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 3)resample for period 3 based on SCF2
        nyears = year_array.shape[0]-1 #counter by excluding the last observed year
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        # NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 3 in "YR1" only => i.e., D only for DJF
        ndays = 365 - m_doye_list[SCF2_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_doy = m_doys_list2[SCF2_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_doy = m_doys_list[SCF2_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = m_doys_list[SCF2_loc[0][0]]-1  #start day
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)

        #================== YR2 ======================
        #make empty arrays to save resampled daily data for YR2
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN
        #make empty arrays to save daily weather data for period 2 in "YR2" only => i.e., J in YR2 only for NDJ
        ndays = m_doye_list[SCF2_loc[0][2]-12]  # Feb in YR2 => 13th index => 13-12=1th index => FEb
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            # if calendar.isleap(year_array[j]): #skip Feb. 29
            #     s_doy = m_doys_list2[SCF1_loc[0][0]]
            Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
            # else: #normal year
            #     s_doy = m_doys_list[SCF1_loc[0][0]]
            #     Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
            #     SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][0:ndays] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 4)resample for period 4 for the remaining months in YR2
        nyears = year_array.shape[0]  
        #make empty arrays to save daily weather data for period 4 (remaining days in YR4)
        ndays = 365 - m_doye_list[SCF2_loc[0][2]-12] #  #check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_date = m_doye_list2[SCF2_loc[0][2]-12]+2 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_date = m_doye_list[SCF2_loc[0][2]-12]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][s_date-1:] = Rain_temp[temp_index,:]
            tmax_resampled[ii][s_date-1:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_date-1:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_date-1:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year+1)

#=============================================================
    #case 3: SCF1 for OND in YR1 and SCF2 for JFM in YR2
#=============================================================
    elif SCF1_loc[0][0] == 9:
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations
        
        #================== YR1 ======================
        #make empty arrays to save daily weather data for period 1 in YR1 before SCF1
        ndays = m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29 [doy = 60]
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 2 based on SCF1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1"
        ndays = m_doye_list[SCF1_loc[0][2]] - m_doye_list[SCF1_loc[0][0]-1]  #e.g., for SON: end day of Nov[334] - end day of Aug[243]   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_doy = 275 # Oct 1 #s_doy = m_doys_list2[SCF1_loc[0][0]]
                e_doy = 366  # e_doy = m_doye_list2[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = 274 # Oct 1 #s_doy = m_doys_list[SCF1_loc[0][0]]
                e_doy = 365   # e_doy = m_doye_list[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = 274 -1 #s_doy = m_doys_list[SCF1_loc[0][0]]-1  #start day index
        e_doy = 365 #e_doy = m_doye_list[SCF1_loc[0][2]]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)

        #================== YR2 ======================
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 3 based on SCF2 in YR2 => JFM
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 3 in "YR2"
        # ndays = m_doye_list[SCF2_loc[0][2]-12] - m_doye_list[SCF2_loc[0][0]-13]  #for FMA: end day of April[120] - end day of Jan[31]   check
        ndays = m_doye_list[2]  #JFM
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                #for Jan1 to Feb 28  => skip Feb 29
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                #from March 1~ March 31
                Rain_temp[j,59:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
                Tmin_temp[j,59:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
                Tmax_temp[j,59:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
            else: #normal year
                e_doy = 90 #March 31 
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR2 matrix with SAMPLES 
        s_doy = 0 # m_doys_list[SCF2_loc[0][0]-12]-1  #start day index
        e_doy = 90 # m_doye_list[SCF2_loc[0][2]-12]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 4)resample for period 4 for the remaining months in YR2
        #make empty arrays to save daily weather data for period 4 (remaining days in YR4)
        ndays = 365 - e_doy #m_doye_list[SCF2_loc[0][2]-12]  #check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): 
                s_date = 92 #April 1 
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_date = 91  #April 1 
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][s_date-1:] = Rain_temp[temp_index,:]
            tmax_resampled[ii][s_date-1:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_date-1:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_date-1:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year+1)
 
#=============================================================
    #case 4: both SCF1 and SCF2 are in YR1, specifically SCF1=JAS, SCF2=OND
#=============================================================
    elif SCF1_loc[0][0] == 6:
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations
        
        #================== YR1 ======================
        #make empty arrays to save daily weather data for period 1 in YR1 before SCF1
        ndays = m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29 [doy = 60]
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 2 based on SCF1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1"
        ndays = m_doye_list[SCF1_loc[0][2]] - m_doye_list[SCF1_loc[0][0]-1]  #e.g., for SON: end day of Nov[334] - end day of Aug[243]   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): 
            if calendar.isleap(year_array[j]): # specifically SCF1=JAS, SCF2=OND
                s_doy = 183 #July 1                 m_doys_list2[SCF1_loc[0][0]]
                e_doy = 274 #Sep. 31     m_doye_list2[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = 182 #July 1  m_doys_list[SCF1_loc[0][0]]
                e_doy = 273 #Sep. 31    m_doye_list[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = 182-1  #July 1 m_doys_list[SCF1_loc[0][0]]-1  #start day index
        e_doy = 273 #Sep. 31    m_doye_list[SCF1_loc[0][2]]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 3)resample for period 3 based on SCF2 in YR1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  # e.g., 36%
        NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 3 in "YR1" only => i.e., D only for DJF
        ndays = 365 - m_doye_list[SCF2_loc[0][0]-1]  #m_doye_list[SCF2_loc[0][2]] - m_doye_list[SCF2_loc[0][0]-1]  #OND check!
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_doy = m_doys_list2[SCF2_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_doy = m_doys_list[SCF2_loc[0][0]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = 274 - 1 #Oct. 1    m_doys_list[SCF2_loc[0][0]]-1  #start day
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)

        #================== YR2 ======================
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations

        ndays = 365  #Entire YR2 is based on climatology
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year+1)

#=============================================================
    #case 5: both SCF1 and SCF2 are in YR2 => Similar to case 4, 
    # but here climatology(p1)+SCF1(p2)+SCF2(p3)+climatology(p4) in YR1, like Kiremt in Ethiopia
#=============================================================
    elif SCF1_loc[0][0] < 6:
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations
        
        #================== YR1 ======================
        #make empty arrays to save daily weather data for period 1 in YR1 before SCF1
        ndays = m_doye_list[SCF1_loc[0][0]-1]
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29 [doy = 60]
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 2 based on SCF1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1"
        ndays = m_doye_list[SCF1_loc[0][2]] - m_doye_list[SCF1_loc[0][0]-1]  #e.g., for SON: end day of Nov[334] - end day of Aug[243]   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): 
            if calendar.isleap(year_array[j]): 
                if SCF1_loc[0][0] == 1:  #if SCF1 for FMA  ==> exclude Feb 29
                    #for Feb 1 to 28th
                    Rain_temp[j,0:28] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    Tmin_temp[j,0:28] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    Tmax_temp[j,0:28] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    SRad_temp[j,0:28] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 32) & (WTD_df["DOY"] <= 59)].values
                    #from March 1~April 30
                    Rain_temp[j,28:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    Tmin_temp[j,28:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    Tmax_temp[j,28:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                    SRad_temp[j,28:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 121)].values
                else:
                    s_doy = m_doys_list2[SCF1_loc[0][0]]
                    e_doy = m_doye_list2[SCF1_loc[0][2]]  #check!
                    Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                    SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = m_doys_list[SCF1_loc[0][0]]
                e_doy = m_doye_list[SCF1_loc[0][2]]  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = m_doys_list[SCF1_loc[0][0]]-1  #start day index
        e_doy = m_doye_list[SCF1_loc[0][2]]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 3)resample for period 3 based on SCF2 in YR1
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  # e.g., 36%
        NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 3 in "YR1" only => i.e., D only for DJF
        ndays = m_doye_list[SCF2_loc[0][2]] - m_doye_list[SCF2_loc[0][0]-1]  #OND check!
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): 
                s_doy = m_doys_list2[SCF2_loc[0][0]]
                e_doy = m_doye_list2[SCF2_loc[0][2]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = m_doys_list[SCF2_loc[0][0]]
                e_doy = m_doye_list[SCF2_loc[0][2]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = m_doys_list[SCF2_loc[0][0]]-1  #start day index
        e_doy = m_doye_list[SCF2_loc[0][2]]
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 4)resample for period 4 for the remaining months in YR1
        #make empty arrays to save daily weather data for period 4 (remaining days in YR4)
        ndays = 365 - e_doy #m_doye_list[SCF2_loc[0][2]-12]  #check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                s_date = m_doye_list2[SCF2_loc[0][2]]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_date = m_doye_list[SCF2_loc[0][2]]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][s_date-1:] = Rain_temp[temp_index,:]
            tmax_resampled[ii][s_date-1:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_date-1:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_date-1:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)

        #================== YR2 ======================
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1 before SCF1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations

        ndays = 365  #Entire YR2 is based on climatology
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year+1)

#=============================================================
    #case 6: both SCF1 and SCF2 are in YR2
#=============================================================
    else:  #case 6: both SCF1 and SCF2 are in YR2
        #================== YR1 fill with climatology ======================
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 1 in YR1, based on climatology
        nyears = year_array.shape[0]
        rand_index = np.random.uniform(0,1,nrealiz) * nyears  #nyears = year_array.shape[0]
        rand_index = rand_index.astype(int)  #0~29 for 30 yrs of observations

        ndays = 365  #Entire YR2 is based on climatology
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): #skip Feb. 29
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Rain_temp[j,59:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmin_temp[j,59:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                Tmax_temp[j,59:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
                SRad_temp[j,59:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= (ndays+1))].values
            else: #normal year
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= ndays)].values

        ## Fill the YR1 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][0:ndays] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][0:ndays] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][0:ndays] = Tmin_temp[temp_index,:]
            srad_resampled[ii][0:ndays] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR2
        #return list of monthly avg of resampled (500) data
        rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, fout1 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year-1)

        #================== YR2: both SCF1 and SCF2 are in YR1=> SCF1=JFM in YR2, SCF2= AMJ ======================
        #make empty arrays to save resampled daily data for YR1
        rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
        tmax_resampled = np.empty((nrealiz,365,))*np.NAN
        tmin_resampled = np.empty((nrealiz,365,))*np.NAN
        srad_resampled = np.empty((nrealiz,365,))*np.NAN

        #++++++++ 1)resample for period 2 based on SCF1=JFM in YR2,
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
        NN = df_scf.iloc[1][SCF1_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in "YR1"
        ndays = m_doye_list[SCF1_loc[0][2]-12]  #JFM   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): 
            if calendar.isleap(year_array[j]): #skip Feb. 29
                #for Jan 1 to Feb 28th
                Rain_temp[j,0:59] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmin_temp[j,0:59] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                Tmax_temp[j,0:59] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                SRad_temp[j,0:59] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= 59)].values
                #from March 1~ March 31
                Rain_temp[j,59:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
                Tmin_temp[j,59:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
                Tmax_temp[j,59:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 61) & (WTD_df["DOY"] <= 91)].values
            else: #normal year
                e_doy = 90 #March 31 
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= 1) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = 0 # m_doys_list[SCF2_loc[0][0]-12]-1  #start day index
        e_doy = 90 # m_doye_list[SCF2_loc[0][2]-12]    #end day index
        for ii in range(0,num_BN):  #0~66
            temp_index = indices1[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices1[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices1[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 2)resample for period 3 based on SCF2(AMJ)
        nyears = year_array.shape[0] 
        BN = df_scf.iloc[0][SCF2_loc[0][0]]  # e.g., 36%
        NN = df_scf.iloc[1][SCF2_loc[0][0]]   #e.g., 33%
        AN = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = nyears//3  #python 3.x => / -> //
        nsample_AN = nyears//3
        nsample_NN = nyears - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #make empty arrays to save daily weather data for period 2 in YR2
        ndays = m_doye_list[SCF2_loc[0][2]-12]- m_doye_list[SCF2_loc[0][0]-13]   #AMJ   check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): 
                s_doy = 92 #April 1     m_doys_list2[SCF2_loc[0][0]]
                e_doy = 182  #June 30    m_doye_list2[SCF2_loc[0][2]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
            else: #normal year
                s_doy = 91 #April 1       m_doys_list[SCF2_loc[0][0]]
                e_doy = 181  #June 30      m_doye_list[SCF2_loc[0][2]]
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_doy) & (WTD_df["DOY"] <= e_doy)].values

        ## Fill the YR1 matrix with SAMPLES 
        s_doy = 91-1   #m_doys_list[SCF2_loc[0][0]]-1  #start day index
        e_doy = 181    #m_doye_list[SCF2_loc[0][2]]
        for ii in range(0,num_BN):  #0~66
            temp_index = indices2[BN_index[ii]] #***********
            rain_resampled[ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = indices2[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = indices2[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Rain_temp[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmax_temp[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][s_doy:e_doy] = Tmin_temp[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][s_doy:e_doy] = SRad_temp[temp_index,:]

        #++++++++ 3)resample for period 3 for the remaining months in YR2
        #make empty arrays to save daily weather data for period 4 (remaining days in YR4)
        ndays = 365 - e_doy #m_doye_list[SCF2_loc[0][2]-12]  #check
        Rain_temp= np.empty((nyears,ndays,))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_temp= np.empty((nyears,ndays,))*np.NAN
        Tmin_temp= np.empty((nyears,ndays,))*np.NAN
        SRad_temp= np.empty((nyears,ndays,))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]): 
                s_date = 183  #July 1           m_doye_list2[SCF2_loc[0][2]]+1 #first DOY after SCF2  #check!
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 366)].values
            else: #normal year
                s_date = 182  #July 1
                Rain_temp[j,0:ndays] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmin_temp[j,0:ndays] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                Tmax_temp[j,0:ndays] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values
                SRad_temp[j,0:ndays] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= s_date) & (WTD_df["DOY"] <= 365)].values

        ## Fill the YR2 matrix with SAMPLES 
        for ii in range(0,nrealiz):  #0~66
            temp_index = indices1[rand_index[ii]] #***********
            rain_resampled[ii][s_date-1:] = Rain_temp[temp_index,:]
            tmax_resampled[ii][s_date-1:] = Tmax_temp[temp_index,:]
            tmin_resampled[ii][s_date-1:] = Tmin_temp[temp_index,:]
            srad_resampled[ii][s_date-1:] = SRad_temp[temp_index,:]

        #====Write monthly WGEN parameter file for the YR1
        rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout2 = write_WGEN_input(rain_resampled, tmax_resampled, tmin_resampled, srad_resampled, wdir, station_name, ALAT, target_year)
    print( '==End of WGEN_PAR - resampling, parameter estimation and writing param_input.txt file for two consecutive years!)')
    
    return rain_res_m1, tmax_res_m1, tmin_res_m1, srad_res_m1, rain_res_m2, tmax_res_m2, tmin_res_m2, srad_res_m2, fout1, fout2
# here       
