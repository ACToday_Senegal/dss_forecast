
# -*- coding: utf-8 -*-
from SIMAGRI._compact import *

class ModuleErrorMessage:

  _UIParent = None

  def __init__(self, parent):
    self._UIParent = parent
    self.__initMessageDialogs()

  def __initMessageDialogs(self):
    self.copy_module_err = Pmw.MessageDialog(self._UIParent, title='Error message in Working Directory',
                                             defaultbutton=0,
                                             message_text='No working directory available!')
    self.copy_module_err.withdraw()

    # error message when there is no scenario name
    self.writeSNX_err = Pmw.MessageDialog(self._UIParent, title='Error message in writing *.SNX',
                                            defaultbutton=0,
                                            message_text='No scenario name available!')
    self.writeSNX_err.withdraw()

    # error message when fertilizer info is missing
    self.fertilizer_err = Pmw.MessageDialog(self._UIParent, title='Error in Fertilizer Input',
                                            defaultbutton=0,
                                            message_text='Fertilizer input missing! Please click the Button for more detiled input on DSSAT setup2 page.')
    self.fertilizer_err.withdraw()
    self.fertilizer_err2 = Pmw.MessageDialog(self._UIParent, title='Error in Fertilizer Input',
                                            defaultbutton=0,
                                            message_text='Numbere of fertilizer application  should be <= 3')
    self.fertilizer_err2.withdraw()
    self.fertilizer_err3 = Pmw.MessageDialog(self._UIParent, title='Error in Fertilizer Input',
                                            defaultbutton=0,
                                            message_text='Numbere of fertilizer application is missing')
    self.fertilizer_err3.withdraw()
    # error message when cultivar info is missing
    self.cultivar_err = Pmw.MessageDialog(self._UIParent, title='Error in Cultivar Input',
                                          defaultbutton=0,
                                          message_text='No Cultivar input! Please click the Button for more detiled input on DSSAT setup1 page.')
    self.cultivar_err.withdraw()
    # error message when irrigation info is missing
    self.irrigation_err = Pmw.MessageDialog(self._UIParent, title='Error in Irrigation Input',
                                            defaultbutton=0,
                                            message_text='No Irrigaiotn input! Please click the Button for more detiled input on DSSAT setup2 page.')
    self.irrigation_err.withdraw()

    # error message when there is no number of fertilizer applications
    self.nfert_err_dialog = Pmw.MessageDialog(self._UIParent, title='Error in fertilizer input',
                                              defaultbutton=0,
                                              message_text='Number of fertilizer applications should be provided in "DSSAT setup 2" tab!')
    self.nfert_err_dialog.iconname('Error in fertilizer input')
    self.nfert_err_dialog.withdraw()

    # error message when there is no number of irrigation (for only irr option=on reported dates)
    self.nirr_err_dialog = Pmw.MessageDialog(self._UIParent, title='Error in fertilizer input',
                                             defaultbutton=0,
                                             message_text='Number of irrigations should be provided if "On Reported dates" is chosen in "DSSAT setup 2" tab!')
    self.nirr_err_dialog.iconname('Error in irrigation input')
    self.nirr_err_dialog.withdraw()


    #EJ(3/6/2020)
    # error message when a trimester for SCF is missing
    self.trimester_err = Pmw.MessageDialog(self._UIParent, title='Error message in selecting a target trimester',
                                            defaultbutton=0,
                                            message_text='No trimseter is selected!')
    self.trimester_err.withdraw()

    # error message when a trimester for SCF is missing
    self.SCF_err = Pmw.MessageDialog(self._UIParent, title='Error message in tercile SCF input',
                                            defaultbutton=0,
                                            message_text='Tercile SCF input is missing')
    self.SCF_err.withdraw()
