# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *


# ==========================Second page "DSSAT Setup 2"
class DSSATSetup2UI:
  __UI_Name__ = loc.DSSATSetup2.Title

  _UIParent = None  ## _single_leading_underscore => private variable
  _UINootbook = None  ## private variable

  _Setting = None

  def __init__(self, setting, parent, notebook):
    print ("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

    # =========================4th page for "DSSAT baseline setup - II "
    page4 = notebook.add('DSSAT setup 2')
    notebook.tab('DSSAT setup 2').focus_set()
    # 1) ADD SCROLLED FRAME
    sf_p4 = Pmw.ScrolledFrame(page4)  # ,usehullsize=1, hull_width=700, hull_height=220)
    sf_p4.pack(padx = 5, pady = 3, fill = 'both', expand = YES)

    # assign frame 1
    fm41 = Frame(sf_p4.interior())
    # Radio button to select "Irrigation method"
    group41 = Pmw.Group(fm41, tag_text = 'Fertilization application', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group41.pack(fill = 'both', expand = 1, side = TOP, padx = 10, pady = 5)

    self._Setting.DSSATSetup2.rbFertApp = tkinter.IntVar()  # self.Fbutton1
    Frt_option = [('Fertilization', 0), ('No Fertilization', 1)]
    for text, value in Frt_option:
      Radiobutton(group41.interior(), text = text, command = self.empty_fert_label,
                  value = value, variable = self._Setting.DSSATSetup2.rbFertApp).pack(side = LEFT, expand = YES)
    self._Setting.DSSATSetup2.rbFertApp.set(1)  # By default- no Fertilization
    # Create button to launch the dialog
    frt_button = tkinter.Button(fm41,
                                text = 'Click to add more details for fertilizer',
                                command=self.getFertInput, bg='gray70').pack(side = TOP, anchor = N)

    # Create the "fertilizer" contents of the page.
    group42 = Pmw.Group(fm41, tag_text = 'Fertilizer application', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group42.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    self._Setting.DSSATSetup2.FA_nfertilizer = Pmw.EntryField(group42.interior(),  # self.nfertilizer
                                      labelpos = 'w', label_text = 'Number of fertilizer applications? ',
                                      validate = {'validator': 'numeric', 'min': 1, 'max': 3, 'minstrict': 0})
    self._Setting.DSSATSetup2.FA_nfertilizer.pack(side = TOP, anchor = W, padx = 5, pady = 5)
    frame_frt = Frame(group42.interior())
    label000 = Label(frame_frt, text = 'No. application', padx = 5, pady = 5)
    label001 = Label(frame_frt, text = 'Days after planting', padx = 5, pady = 5)
    label002 = Label(frame_frt, text = 'Amount (N, kg/ha)', padx = 5, pady = 5)
    label003 = Label(frame_frt, text = 'Fertilizer material', padx = 5, pady = 5)
    label004 = Label(frame_frt, text = 'Application method', padx = 5, pady = 5)
    self.label005 = Label(frame_frt, text = '1st:', padx = 5, pady = 5)
    self.label006 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label007 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=15)
    self.label008 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label009 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=35)
    self.label010 = Label(frame_frt, text = '2nd:', padx = 5, pady = 5)
    self.label011 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label012 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=15)
    self.label013 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label014 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=35)
    self.label015 = Label(frame_frt, text = '3rd:', padx = 5, pady = 5)
    self.label016 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label017 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=15)
    self.label018 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label019 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=35)
    # aline  empty label
    entries = (label000, label001, label002, label003, label004)
    i = 0
    for entry in entries:
      entry.grid(row=0, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label005, self.label006, self.label007, self.label008, self.label009)
    i = 0
    for entry in entries:
      entry.grid(row=1, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label010, self.label011, self.label012, self.label013, self.label014)
    i = 0
    for entry in entries:
      entry.grid(row=2, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label015, self.label016, self.label017, self.label018, self.label019)
    i = 0
    for entry in entries:
      entry.grid(row=3, column=i)  # , sticky=W)
      i = i + 1
    frame_frt.pack(fill = 'x', expand = 1, side = TOP)
    fm41.pack(fill = 'both', expand = 1, side = TOP)
    # assign frame 2
    fm42 = Frame(sf_p4.interior())
    # Radio button to select "Planting method"
    group43 = Pmw.Group(fm42, tag_text = 'Irrigation', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group43.pack(fill = 'x', expand = 1, side = TOP, padx = 10, pady = 5)

    self._Setting.DSSATSetup2.rbIrrigation = tkinter.IntVar()  # self.IRbutton
    IR_option = [('Automatic when required', 0), ('No Irrigation', 1), ('Irrigation', 2)]
    for text, value in IR_option:
      # Radiobutton(group43.interior(), text = text, command=self.empty_irrig_label, value = value, variable = self._Setting.DSSATSetup2.rbIrrigation).pack(
      #   side = LEFT, expand = YES)
      Radiobutton(group43.interior(), text = text, value = value, variable = self._Setting.DSSATSetup2.rbIrrigation).pack(
        side = LEFT, expand = YES)
    self._Setting.DSSATSetup2.rbIrrigation.set(1)  # By default- no irrigation

    # set up "irrigation method"
    group44 = Pmw.Group(fm42, tag_text = 'Irrigation method',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group44.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    Irr_meth_list = ("IR001(Furrow)","IR003(Flood)","IR004(Sprinkler)")
    self._Setting.DSSATSetup2.irr_method = Pmw.ComboBox(group44.interior(), label_text='Irrigation method:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=Irr_meth_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup2.irr_method.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    self._Setting.DSSATSetup2.irr_method.selectitem(Irr_meth_list[0])
    fm42.pack(fill = 'x', expand = 1, side = TOP)

    # # Create button to launch the dialog
    # Irr_button = tkinter.Button(fm42,
    #                             text = 'Click to add more details for irrigation',
    #                             command=self.getIrrInput, bg='gray70').pack(side = TOP, anchor = N)

    # # Create the "Automatic irrigation" contents of the page.
    # group44 = Pmw.Group(fm42, tag_text = 'Irrigation (Automatic)', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    # group44.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    # self.label401 = Label(group44.interior(), text = 'Management depth(cm):', anchor = W, padx = 10)
    # self.label401.grid(row=0, column=0, sticky=W)
    # self.label402 = Label(group44.interior(), text = loc.Msg.Not_added, relief='sunken', padx = 5)
    # self.label402.grid(row=0, column=1, sticky=W)
    # self.label403 = Label(group44.interior(), text = 'Threshold(% of max available):', anchor = W, padx = 10)
    # self.label403.grid(row=0, column=2, sticky=W)
    # self.label404 = Label(group44.interior(), text = loc.Msg.Not_added, relief='sunken', padx = 5)
    # self.label404.grid(row=0, column=3, sticky=W)
    # ##        self.label405 = Label(group44.interior(), text = 'End point(% of max available):',anchor = W, padx = 10)
    # ##        self.label405.grid(row=1,column=0, sticky=W)
    # ##        self.label406 = Label(group44.interior(), text = loc.Msg.Not_added,relief='sunken',padx = 5)
    # ##        self.label406.grid(row=1,column=1, sticky=W)
    # self.label407 = Label(group44.interior(), text = 'Efficiency fraction:', anchor = W, padx = 10)
    # self.label407.grid(row=1, column=0, sticky=W)
    # self.label408 = Label(group44.interior(), text = loc.Msg.Not_added, relief='sunken', padx = 5)
    # self.label408.grid(row=1, column=1, sticky=W)

    # # Create the "manual irrigation" contents of the page.
    # group45 = Pmw.Group(fm42, tag_text = 'Irrigation (Reported)', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    # group45.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    # self._Setting.DSSATSetup2.IR_nirrigation = Pmw.EntryField(group45.interior(),  # self.nirrigation
    #                                   labelpos = 'w', label_text = 'Number of irrigations? ',
    #                                   validate = {'validator': 'numeric', 'min': 1, 'max': 3, 'minstrict': 0})
    # self._Setting.DSSATSetup2.IR_nirrigation.pack(side = TOP, padx = 5, anchor = W, pady = 5)
    # frame_in1 = Frame(group45.interior())
    # self.label120 = Label(frame_in1, text = 'Puddling date(YYDOY):', padx = 5)
    # self.label120.grid(row=0, column=0, sticky=W)
    # self.label121 = Label(frame_in1, text = loc.Msg.Not_added, relief='sunken', width=13)
    # self.label121.grid(row=0, column=1, sticky=W)
    # ##        self.label122 = Label(frame_in1, text = 'Puddling:',padx = 5)
    # ##        self.label122.grid(row=1,column=0, sticky=W)
    # ##        self.label123= Label(frame_in1, text = loc.Msg.Not_added,relief='sunken',width=13)
    # ##        self.label123.grid(row=1,column=1, sticky=W)
    # self.label124 = Label(frame_in1, text = 'Percolation rate(mm/day):', padx = 5)
    # self.label124.grid(row=2, column=0, sticky=W)
    # self.label125 = Label(frame_in1, text = loc.Msg.Not_added, relief='sunken', width=13)
    # self.label125.grid(row=2, column=1, sticky=W)
    # frame_in1.pack(fill = 'x', expand = 1, side = TOP)

    # frame_in2 = Frame(group45.interior())
    # label100 = Label(frame_in2, text = 'No. irrigation', width=10, padx = 5, pady = 5)
    # label101 = Label(frame_in2, text = 'Date(YYDOY)', width=13, padx = 5, pady = 5)
    # label102 = Label(frame_in2, text = 'Bund height', width=15, padx = 5, pady = 5)
    # label103 = Label(frame_in2, text = 'Flood depth', width=20, padx = 5, pady = 5)
    # label1031 = Label(frame_in2, text = 'Constant depth?', width=20, padx = 5, pady = 5)
    # self.label104 = Label(frame_in2, text = '1st:', width=10, padx = 5)
    # self.label105 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=13)
    # self.label106 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=15)
    # self.label107 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # self.label1071 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # self.label108 = Label(frame_in2, text = '2nd:', width=10, padx = 5)
    # self.label109 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=13)
    # self.label110 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=15)
    # self.label111 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # self.label1111 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # self.label112 = Label(frame_in2, text = '3rd:', width=10, padx = 5)
    # self.label113 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=13)
    # self.label114 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=15)
    # self.label115 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # self.label1151 = Label(frame_in2, text = loc.Msg.Not_added, relief='sunken', width=20)
    # # aline  empty label
    # entries = (label100, label101, label102, label103, label1031)
    # i = 0
    # for entry in entries:
    #   entry.grid(row=0, column=i)  # , sticky=W)
    #   i = i + 1
    # entries = (self.label104, self.label105, self.label106, self.label107, self.label1071)
    # i = 0
    # for entry in entries:
    #   entry.grid(row=1, column=i)  # , sticky=W)
    #   i = i + 1
    # entries = (self.label108, self.label109, self.label110, self.label111, self.label1111)
    # i = 0
    # for entry in entries:
    #   entry.grid(row=2, column=i)  # , sticky=W)
    #   i = i + 1
    # entries = (self.label112, self.label113, self.label114, self.label115, self.label1151)
    # i = 0
    # for entry in entries:
    #   entry.grid(row=3, column=i)  # , sticky=W)
    #   i = i + 1
    # frame_in2.pack(fill = 'x', expand = 1, side = TOP)

    # fm42.pack(fill = 'x', expand = 1, side = TOP)
    #   fm_in1=Frame(fm31)

    # init. Dialogs
    self.__initDialogs()


  def __initDialogs(self):
    # ============to add more specification for fertilization information
    self.fert_dialog = Pmw.Dialog(self._UIParent, title = 'Fertilization Application')
    self.frt_group1 = Pmw.Group(self.fert_dialog.interior(), tag_text = '1st application')
    self.frt_group1.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day1 = Pmw.EntryField(self.frt_group1.interior(),  # self.day1
                               labelpos = 'w', label_text = 'Days after planting:',
                               validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0})
    self._Setting.DSSATSetup2.FA_amount1 = Pmw.EntryField(self.frt_group1.interior(),  # self.amount1
                                     labelpos = 'w', label_text = 'Amount (N, kg/ha):',
                                     validate = {'validator': 'real', 'min': 1, 'max': 100, 'minstrict': 0})
    # -fertilizer material
    material_list = ("FE001(Ammonium nitrate)","FE004(Anhydrous ammonia)","FE005(Urea)",
                    "FE006(Diammnoium phosphate)","FE0010(Urea ammonium nitrate solution)","None")
    self._Setting.DSSATSetup2.FA_fert_mat1 = Pmw.ComboBox(self.frt_group1.interior(),  # fert_mat1
                                     label_text = 'fertilizer material:', labelpos = 'wn',
                                     listbox_width = 15, dropdown = 1,
                                     scrolledlist_items = material_list,
                                     entryfield_entry_state = DISABLED)
    first = material_list[5]
    self._Setting.DSSATSetup2.FA_fert_mat1.selectitem(first)
    # -fertilizer application method
    apply_list = ("AP001(Broadcast, not incorporated)","AP002(Broadcast, incorporated)",
                  "AP003(Banded on surface)","AP004(Banded beneath surface)",
                  "AP011(Broadcast on flooded/saturated soil, none in soil)","None")
    self._Setting.DSSATSetup2.FA_fert_app_method1 = Pmw.ComboBox(self.frt_group1.interior(),  # fert_app1
                                                                 label_text = 'application method:', labelpos = 'wn',
                                                                 listbox_width = 15, dropdown = 1,
                                                                 scrolledlist_items = apply_list,
                                                                 entryfield_entry_state = DISABLED)
    first = apply_list[5]
    self._Setting.DSSATSetup2.FA_fert_app_method1.selectitem(first)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day1, self._Setting.DSSATSetup2.FA_amount1,
               self._Setting.DSSATSetup2.FA_fert_mat1, self._Setting.DSSATSetup2.FA_fert_app_method1)
    for entry in entries:
      entry.pack(fill = 'x', side = TOP, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)

    # 2nd fertilizer application
    self.frt_group2 = Pmw.Group(self.fert_dialog.interior(), tag_text = '2nd application')
    self.frt_group2.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day2 = Pmw.EntryField(self.frt_group2.interior(),   # day2
                                  labelpos = 'w', label_text = 'Days after planting:',
                                  validate = {'validator': 'numeric', 'min': 1, 'max': 200, 'minstrict': 0})
    self._Setting.DSSATSetup2.FA_amount2 = Pmw.EntryField(self.frt_group2.interior(),  # amount2
                                     labelpos = 'w', label_text = 'Amount(N, kg/ha):',
                                     validate = {'validator': 'real', 'min': 1, 'max': 100, 'minstrict': 0})
    # -fertilizer material
    self._Setting.DSSATSetup2.FA_fert_mat2 = Pmw.ComboBox(self.frt_group2.interior(),  # fert_mat2
                                     label_text = 'fertilizer material:', labelpos = 'wn',
                                     listbox_width = 15, dropdown = 1,
                                     scrolledlist_items = material_list,
                                     entryfield_entry_state = DISABLED)
    first = material_list[5]
    self._Setting.DSSATSetup2.FA_fert_mat2.selectitem(first)
    # -fertilizer application method
    self._Setting.DSSATSetup2.FA_fert_app_method2 = Pmw.ComboBox(self.frt_group2.interior(),  # fert_app2
                                            label_text = 'application method:', labelpos = 'wn',
                                            listbox_width = 15, dropdown = 1,
                                            scrolledlist_items = apply_list,
                                            entryfield_entry_state = DISABLED)
    first = apply_list[5]
    self._Setting.DSSATSetup2.FA_fert_app_method2.selectitem(first)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day2, self._Setting.DSSATSetup2.FA_amount2,
               self._Setting.DSSATSetup2.FA_fert_mat2, self._Setting.DSSATSetup2.FA_fert_app_method2)
    for entry in entries:
      entry.pack(fill = 'x', side = TOP, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    # 3rd fertilizer application
    self.frt_group3 = Pmw.Group(self.fert_dialog.interior(), tag_text = '3rd application')
    self.frt_group3.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day3 = Pmw.EntryField(self.frt_group3.interior(),  # day3
                                  labelpos = 'w', label_text = 'Days after planting:',
                                  validate = {'validator': 'numeric', 'min': 1, 'max': 200, 'minstrict': 0})
    self._Setting.DSSATSetup2.FA_amount3 = Pmw.EntryField(self.frt_group3.interior(),  # amount3
                                     labelpos = 'w', label_text = 'Amount(N, kg/ha):',
                                     validate = {'validator': 'real', 'min': 1, 'max': 100, 'minstrict': 0})
    # -fertilizer material
    self._Setting.DSSATSetup2.FA_fert_mat3 = Pmw.ComboBox(self.frt_group3.interior(), # fert_math3
                                     label_text = 'fertilizer material:', labelpos = 'wn',
                                     listbox_width = 15, dropdown = 1,
                                     scrolledlist_items = material_list,
                                     entryfield_entry_state = DISABLED)
    first = material_list[5]
    self._Setting.DSSATSetup2.FA_fert_mat3.selectitem(first)
    # -fertilizer application method
    self._Setting.DSSATSetup2.FA_fert_app_method3 = Pmw.ComboBox(self.frt_group3.interior(), # fert_app3
                                            label_text = 'application method:', labelpos = 'wn',
                                            listbox_width = 15, dropdown = 1,
                                            scrolledlist_items = apply_list,
                                            entryfield_entry_state = DISABLED)
    first = apply_list[5]
    self._Setting.DSSATSetup2.FA_fert_app_method3.selectitem(first)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day3, self._Setting.DSSATSetup2.FA_amount3,
               self._Setting.DSSATSetup2.FA_fert_mat3, self._Setting.DSSATSetup2.FA_fert_app_method3)
    for entry in entries:
      entry.pack(fill = 'x', side = TOP, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    self.fert_dialog.withdraw()

    # ============to add more specification for automatic irrigation(auto when required)
    # self.AutoIrrDialog = Pmw.Dialog(self._UIParent, title = 'Automatic irrigation when required')
    # self._Setting.DSSATSetup2.IA_mng_depth = Pmw.EntryField(self.AutoIrrDialog.interior(),  # irr_depth
    #                                 labelpos = 'w', label_text = 'Management depth(cm):',
    #                                 validate = {'validator': 'real', 'min': 1, 'max': 200, 'minstrict': 0},
    #                                 value = '30')
    # # -threshold
    # self._Setting.DSSATSetup2.IA_threshold = Pmw.EntryField(self.AutoIrrDialog.interior(),  # self.irr_thresh
    #                                  labelpos = 'w', label_text = 'Threshold(% of maximum available water triggering irrigation ):',
    #                                  validate = {'validator': 'real', 'min': 1, 'max': 100, 'minstrict': 0},
    #                                  value = '50')
    # # - Efficiency fraction
    # self._Setting.DSSATSetup2.IA_eff_fraction = Pmw.EntryField(self.AutoIrrDialog.interior(),  # eff_fraction
    #                                                             labelpos = 'w', label_text = 'Efficiency fraction:',
    #                                                             validate = {'validator': 'real', 'min': 0, 'max': 1, 'minstrict': 0},
    #                                                             value = '1')
    # # aline labels
    # entries = (self._Setting.DSSATSetup2.IA_mng_depth, self._Setting.DSSATSetup2.IA_threshold, self._Setting.DSSATSetup2.IA_eff_fraction)
    # for entry in entries:
    #   entry.pack(fill = 'x', expand = 1, padx = 10, pady = 5)
    # Pmw.alignlabels(entries)
    # self.AutoIrrDialog.withdraw()

  def empty_fert_label(self):
    self._Setting.DSSATSetup2.FA_nfertilizer.clear()
    self.label006.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label007.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label008.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label009.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label011.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label012.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label013.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label014.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label016.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label017.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label018.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
    self.label019.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')

  # def empty_irrig_label(self):
  #   self.label402.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label404.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label408.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label121.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #  # self.label123.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label125.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label105.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label106.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label107.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label1071.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label109.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label110.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label111.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label1111.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label113.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label114.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label115.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')
  #   self.label1151.configure(text = loc.Msg.Not_added, bg = 'SystemButtonFace')


  # #Get irrigationinformation
  # def getIrrInput(self):
  #     if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  #Automatic when required
  #         self.AutoIrrDialog.activate()
  #         self.label402.configure(text = self._Setting.DSSATSetup2.IA_mng_depth.getvalue(), background = 'honeydew1')
  #         self.label404.configure(text = self._Setting.DSSATSetup2.IA_threshold.getvalue(), background = 'honeydew1')
  #         self.label408.configure(text = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue(), background = 'honeydew1')

  #     elif self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
  #         self.ReptIrrDialog.activate()
  #         # print 'constant flooding depth?  ', \
  #         #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth1, \
  #         #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth2, \
  #         #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth3   #floodVar1, floodVar2, floodVar3

  #         if self._Setting.DSSATSetup2.IR_irr_day1.getvalue() != "None":
  #             self.label105.configure(text = self._Setting.DSSATSetup2.IR_irr_day1.getvalue(), background = 'honeydew1')
  #             self.label106.configure(text = self._Setting.DSSATSetup2.IR_irr_height1.getvalue(), background = 'honeydew1')
  #             self.label107.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth1.getvalue(), background = 'honeydew1')
  #             self.label121.configure(text = self._Setting.DSSATSetup2.IR_pud_date.getvalue(), background = 'honeydew1')
  #            # self.label123.configure(text = self.puddling.getvalue(), background = 'honeydew1')
  #             self.label125.configure(text = self._Setting.DSSATSetup2.IR_pud_pec_rate.getvalue(), background = 'honeydew1')
  #             if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth1.get() == 0:  # floodVar1
  #                 self.label1071.configure(text = 'Yes', background = 'honeydew1')
  #             else:
  #                 self.label1071.configure(text = 'No', background = 'honeydew1')

  #         if self._Setting.DSSATSetup2.IR_irr_day2.getvalue() != "None":
  #             self.label109.configure(text = self._Setting.DSSATSetup2.IR_irr_day2.getvalue(), background = 'honeydew1')
  #             self.label110.configure(text = self._Setting.DSSATSetup2.IR_irr_height2.getvalue(), background = 'honeydew1')
  #             self.label111.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth2.getvalue(), background = 'honeydew1')
  #             if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth2.get() == 0:  # floodVar2
  #                 self.label1111.configure(text = 'Yes', background = 'honeydew1')
  #             else:
  #                 self.label1111.configure(text = 'No', background = 'honeydew1')

  #         if self._Setting.DSSATSetup2.IR_irr_day3.getvalue() != "None":  # irr_day3
  #             self.label113.configure(text = self._Setting.DSSATSetup2.IR_irr_day3.getvalue(), background = 'honeydew1')
  #             self.label114.configure(text = self._Setting.DSSATSetup2.IR_irr_height3.getvalue(), background = 'honeydew1')
  #             self.label115.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth3 .getvalue(), background = 'honeydew1')
  #             if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth3.get() == 0:  # floodVar3
  #                 self.label1151.configure(text = 'Yes', background = 'honeydew1')
  #             else:
  #                 self.label1151.configure(text = 'No', background = 'honeydew1')

  #Get fertilizer application information
  def getFertInput(self):
    if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
      self._Setting.DSSATSetup2.FA_nfertilizer.clear()
      self.fert_dialog.activate()
      if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != "None":
        self.label006.configure(text = self._Setting.DSSATSetup2.FA_day1.getvalue(), background = 'honeydew1')
        self.label007.configure(text = self._Setting.DSSATSetup2.FA_amount1.getvalue(), background = 'honeydew1')
        self.label008.configure(text = self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0], background = 'honeydew1')
        self.label009.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0], background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != "None":
        self.label011.configure(text = self._Setting.DSSATSetup2.FA_day2.getvalue(), background = 'honeydew1')
        self.label012.configure(text = self._Setting.DSSATSetup2.FA_amount2.getvalue(), background = 'honeydew1')
        self.label013.configure(text = self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0], background = 'honeydew1')
        self.label014.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0], background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != "None":
        self.label016.configure(text = self._Setting.DSSATSetup2.FA_day3.getvalue(), background = 'honeydew1')
        self.label017.configure(text = self._Setting.DSSATSetup2.FA_amount3.getvalue(), background = 'honeydew1')
        self.label018.configure(text = self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0], background = 'honeydew1')
        self.label019.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0], background = 'honeydew1')


  # initialize UI (user interface) with Setting
  def initUI_Setting(self):
    temp = self._Setting.DSSATSetup2.FA_nfertilizer.getvalue()  #(EJ: 2/24/2020)
    # Fertilization application
    self.empty_fert_label()

    if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
      #load no. of fertilizer application from the saved config (EJ: 2/24/2020)
      # self._Setting.DSSATSetup2.FA_nfertilizer.setentry(self._Setting.DSSATSetup2.FA_nfertilizer.getvalue())
      self._Setting.DSSATSetup2.FA_nfertilizer.setentry(temp)
      if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != "None":
        self.label006.configure(text = self._Setting.DSSATSetup2.FA_day1.getvalue(), background = 'honeydew1')
        self.label007.configure(text = self._Setting.DSSATSetup2.FA_amount1.getvalue(), background = 'honeydew1')
        self.label008.configure(text = self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0], background = 'honeydew1')
        self.label009.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0],
                                background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != "None":
        self.label011.configure(text = self._Setting.DSSATSetup2.FA_day2.getvalue(), background = 'honeydew1')
        self.label012.configure(text = self._Setting.DSSATSetup2.FA_amount2.getvalue(), background = 'honeydew1')
        self.label013.configure(text = self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0], background = 'honeydew1')
        self.label014.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0],
                                background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != "None":
        self.label016.configure(text = self._Setting.DSSATSetup2.FA_day3.getvalue(), background = 'honeydew1')
        self.label017.configure(text = self._Setting.DSSATSetup2.FA_amount3.getvalue(), background = 'honeydew1')
        self.label018.configure(text = self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0], background = 'honeydew1')
        self.label019.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0],
                                background = 'honeydew1')

    # # Irrigation
    # self.empty_irrig_label()

    # if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  # Automatic when required
    #   self.label402.configure(text = self._Setting.DSSATSetup2.IA_mng_depth.getvalue(), background = 'honeydew1')
    #   self.label404.configure(text = self._Setting.DSSATSetup2.IA_threshold.getvalue(), background = 'honeydew1')
    #   self.label408.configure(text = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue(), background = 'honeydew1')

    # elif self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  # on reported dates
    #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth1, \
    #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth2, \
    #   self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth3  # floodVar1, floodVar2, floodVar3

    #   self.label121.configure(text = self._Setting.DSSATSetup2.IR_pud_date.getvalue(), background = 'honeydew1')
    #   # self.label123.configure(text = self.puddling.getvalue(), background = 'honeydew1')
    #   self.label125.configure(text = self._Setting.DSSATSetup2.IR_pud_pec_rate.getvalue(), background = 'honeydew1')

    #   if self._Setting.DSSATSetup2.IR_irr_day1.getvalue() != "None":
    #     self.label105.configure(text = self._Setting.DSSATSetup2.IR_irr_day1.getvalue(), background = 'honeydew1')
    #     self.label106.configure(text = self._Setting.DSSATSetup2.IR_irr_height1.getvalue(), background = 'honeydew1')
    #     self.label107.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth1.getvalue(), background = 'honeydew1')
    #     if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth1.get() == 0:  # floodVar1
    #       self.label1071.configure(text = 'Yes', background = 'honeydew1')
    #     else:
    #       self.label1071.configure(text = 'No', background = 'honeydew1')

    #   if self._Setting.DSSATSetup2.IR_irr_day2.getvalue() != "None":
    #     self.label109.configure(text = self._Setting.DSSATSetup2.IR_irr_day2.getvalue(), background = 'honeydew1')
    #     self.label110.configure(text = self._Setting.DSSATSetup2.IR_irr_height2.getvalue(), background = 'honeydew1')
    #     self.label111.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth2.getvalue(), background = 'honeydew1')
    #     if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth2.get() == 0:  # floodVar2
    #       self.label1111.configure(text = 'Yes', background = 'honeydew1')
    #     else:
    #       self.label1111.configure(text = 'No', background = 'honeydew1')

    #   if self._Setting.DSSATSetup2.IR_irr_day3.getvalue() != "None":  # irr_day3
    #     self.label113.configure(text = self._Setting.DSSATSetup2.IR_irr_day3.getvalue(), background = 'honeydew1')
    #     self.label114.configure(text = self._Setting.DSSATSetup2.IR_irr_height3.getvalue(), background = 'honeydew1')
    #     self.label115.configure(text = self._Setting.DSSATSetup2.IR_irr_flood_depth3.getvalue(), background = 'honeydew1')
    #     if self._Setting.DSSATSetup2.rbIR_irr_const_flood_depth3.get() == 0:  # floodVar3
    #       self.label1151.configure(text = 'Yes', background = 'honeydew1')
    #     else:
    #       self.label1151.configure(text = 'No', background = 'honeydew1')