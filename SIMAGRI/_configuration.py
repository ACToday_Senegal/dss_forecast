# -*- coding: utf-8 -*-

from SIMAGRI._compact import *

if PY2:
    import ConfigParser
else:
    import configparser

# __author__ = "Seongkyu Lee"
__author__ = "Eunjin Han"

_simagri_configuration_file = "SIMAGRI.cfg"


CONFIG = None
if PY2:
  CONFIG = ConfigParser.SafeConfigParser()
else:
  CONFIG = configparser.ConfigParser()

CONFIG_FILE = path.join(CurCWD, _simagri_configuration_file)

CONFIG_LOADED = False

# load configuration file
def load_configuration(cfgfile = None):

  if cfgfile is None:
    cfgfile = CONFIG_FILE

  loaded_files = CONFIG.read(cfgfile)
  if loaded_files:
    CONFIG_LOADED = True
    print("Load variables' values in %s" % (cfgfile))
  else:
    CONFIG_LOADED = False
    print('No configuration files loaded. Using default values')

  # TODO: implement set default values in configration file
  if CONFIG_LOADED == False:
    pass

# save configuration file
def save_configuration(cfgfile = None):
  if cfgfile is None:
    cfgfile = CONFIG_FILE

  print("Save variables' values in %s" % (cfgfile))

  if CONFIG is not None:
    with open(cfgfile, 'w') as configfile:
      CONFIG.write(configfile)

def get_config_value(section, option, validation = None):
  if not CONFIG:
    load_configuration()

  value = None

  if CONFIG.has_section(section):
    if CONFIG.has_option(section, option):
      value = CONFIG.get(section, option)

      # Convert Boolean string to real Boolean values
      """
      if value.lower() == "false":
        value = False
      elif value.lower() == "true":
        value = True
     """

  if validation is not None:
    flag_valid = True

    for valid in validation:
      if value == valid:
        flag_valid = True
        break

    if flag_valid == False:
      value = None

  if value is not None:
    value = value.replace('%%', '%')

  return value


def set_config_value(section, option, value, make=False):
  if not CONFIG:
    load_configuration()

  if make == True:
    if CONFIG.has_section(section) == False:
      CONFIG.add_section(section)

  if CONFIG.has_section(section):
    #if CONFIG.has_option(section, option):
    # Convert real Boolean values to Boolean string
    if isinstance(value, str) == False:
      value = str(value)
    """
    if value == False:
      value = 'false'
    elif value == True:
      value = 'true'
    """

    value = value.replace('%', '%%')

    CONFIG.set(section, option, value)

  return True


def get_config_value_into_string(section, option, defaultValue = ''):
  value = get_config_value(section, option)
  if value is None:
    value = defaultValue
  return value

def get_config_value_into_tkinter_intvar(section, option, component):
  value = get_config_value(section, option)
  if value is not None:
    if isinstance(value, str) == True:
      value = int(value)
    component.set(value)

def get_config_value_into_pmw_entryfield(section, option, component):
  value = get_config_value(section, option)
  if value is not None:
    component.setvalue(value)


def get_config_value_into_pmw_combobox(section, option, component):
  value = get_config_value(section, option)
  if value is not None:
    component.selectitem(value)

def get_config_value_into_tkinter_label(section, option, component):
  value = get_config_value(section, option)
  if value is not None:
    component.configure(text = value, background = 'honeydew1')


# set

def set_config_value_from_string(section, option, component):
  if component is not None:
    # set_config_value(section, option, component, make = True)
    set_config_value(section, option, component, make = True)

def set_config_value_from_tkinter_intvar(section, option, component):
  if component is not None:
    set_config_value(section, option, component.get(), make = True)

def set_config_value_from_pmw_entryfield(section, option, component):
  if component is not None:
    set_config_value(section, option, component.getvalue(), make = True)

def set_config_value_from_pmw_combobox(section, option, component):
  if component is not None:
    set_config_value(section, option, component.getvalue()[0], make = True)

def set_config_value_from_tkinter_label(section, option, component):
  if component is not None:
    set_config_value(section, option, component.cget("text"), make = True)
